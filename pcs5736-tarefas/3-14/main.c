#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(void) 
{

    int a, b, c, x, y, z;
    a = 2;
    b = 3;
    c = 10;

    #pragma omp parallel num_threads(3) default(shared)
    {
        int tid = omp_get_thread_num();
        #pragma omp barrier

        if (tid == 0){
            x = a + b + c*2;
        }
        
        if (tid == 1){
            y = b - a + c;
        }
        
        if (tid == 2){
            z = a + c - b;
        }
        
    }


    printf("Valor de x = %d\n", x);
    printf("Valor de y = %d\n", y);
    printf("Valor de z = %d\n", z);

    return 0;
}
