#!/bin/bash

gcc -fopenmp main.c
export OMP_NUM_THREADS=16
export OMP_NESTED=true
./a.out
