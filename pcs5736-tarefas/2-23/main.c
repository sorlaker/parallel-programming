#include <stdio.h>
#include <stdlib.h>



void multi_arr(int size, double * a, double * b, double * c){
    int i, j, k;
    #pragma omp parallel for default(shared) private(i, j, k)
    for (i = 0; i < size; i++)
        for (j = 0; j < size; j++)
            for (k = 0; k < size; k++)
                c[i*size + j] += a[i*size + k] * b[i*size + k];
}
void sum_arr(int size, double * a, double * b, double * c){
    int i, j;
    #pragma omp parallel for default(shared) private(i, j)
    for (i = 0; i < size; i++)
        for (j = 0; j < size; j++)
            c[i*size + j] += a[i*size + j] + b[i*size + j];
}
void print_arr(double * a){
    int i;
    for (i = 0; i < 10; i++)
        printf("%lf ", a[i]);
    printf("\n");
}
int main(void){

    int size = 2500;
    double * array_a = (double *) malloc(size*size*sizeof(double));
    double * array_b = (double *) malloc(size*size*sizeof(double));
    double * array_c = (double *) malloc(size*size*sizeof(double));
    double * array_d = (double *) malloc(size*size*sizeof(double));
    
    int i, j;
    #pragma omp parallel for collapse(2)
    for (i = 0; i < size; i++){
        for (j = 0; j < size; j++){
            array_a[i*size + j] = i+j;
            array_b[i*size + j] = i+2*j;
            array_c[i*size + j] = 2*i+3*j;
            array_d[i*size + j] = 2*i+j;
        }
    }

    // FINAL = A * B + C * D
    double * array_rs_final = (double *) calloc(size*size, sizeof(double));
    

    #pragma omp parallel default(shared)
    #pragma omp single nowait
    {
        printf("Started\n"); fflush(0);
        double * array_rs1 = (double *) calloc(size*size, sizeof(double));
        double * array_rs2 = (double *) calloc(size*size, sizeof(double));
        
        #pragma omp task shared(array_rs1)
        multi_arr(size, array_a, array_b, array_rs1);                
        #pragma omp task shared(array_rs2)
        multi_arr(size, array_c, array_d, array_rs2);                

        #pragma omp taskwait

        printf("Finished\n");
        sum_arr(size, array_rs1, array_rs2, array_rs_final);
    }

    printf("Finished omp\n");

    print_arr(array_rs_final);
    



    return 0;

}
