#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>


int main(void) 
{

    omp_lock_t writelock;

    omp_init_lock(&writelock);

    #pragma omp parallel num_threads(2)
    {
        int tid = omp_get_thread_num();
        for ( int i = 0; i < 1000; i+=50 )
        {
            sleep(1);
            omp_set_lock(&writelock);
            struct timeval tv;
            gettimeofday(&tv, NULL);
            printf("%ld thread id %d i = %d\n", tv.tv_sec, tid, i); fflush(0);
            sleep(1);
            //printf("thread id %d i = %d\n", tid, i); fflush(0);
            omp_unset_lock(&writelock);
        }

        omp_destroy_lock(&writelock);



    }
        return 0;
}
