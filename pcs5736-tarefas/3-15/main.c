#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void) 
{
    int * a = (int *) malloc(1000*1000*sizeof(int));

    int i, j;
    for (i = 0; i < 1000; i++)
        for (j = 0; j < 1000; j++)
            a[i*1000 + j] = i*1000 + j;

    float sum = 0;
    #pragma omp parallel for reduction(+:sum) private(i, j)
    for (i = 0; i < 1000; i++){
        for (j = 0; j < 1000; j++){
            sum += a[i*1000 + j];
        }
    }
    
    float variancia = sum/1000*1000;
    printf("variancia = %f\n", variancia);
    printf("desvio = %f\n", sqrt(variancia));


    return 0;
}
