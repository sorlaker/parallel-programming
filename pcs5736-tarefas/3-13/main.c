#include <stdio.h>
#include <stdlib.h>

int main(void) 
{
    int * a = (int *) malloc(1000*1000*sizeof(int));

    int i, j;
    for (i = 0; i < 1000; i++)
        for (j = 0; j < 1000; j++)
            a[i*1000 + j] = 1;

    int sum = 0;
    #pragma omp parallel for reduction(+:sum) private(i, j)
    for (i = 0; i < 1000; i++){
        for (j = 0; j < 1000; j++){
            sum += a[i*1000 + j];
        }
    }
    printf("sum = %d\n", sum);


    return 0;
}
