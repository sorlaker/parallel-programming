#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	// Get the name of the processor
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	// Print off a hello world message
	printf("Hello world from processor %s, rank %d"
			" out of %d processors\n",
			processor_name, world_rank, world_size);
	//int a[500][500], b[500][500], c[500][500], d[500][500], e[500][500];
	int i, j, k;
	double W[500][500], T[500][500], X[500][500], Y[500][500], Z[500][500], A[500][500], B[500][500], C[500][500], D[500][500], E[500][500], F[500][500];
	for (i = 0; i < 500; i++)
		for (j = 0; j < 500; j++){
			A[i][j] = 5;
			B[i][j] = 5;
			C[i][j] = 5;
			D[i][j] = 5;
			E[i][j] = 5;
			F[i][j] = 5;
			T[i][j] = 5;
			X[i][j] = 0;
			Y[i][j] = 0;
			Z[i][j] = 0; 
		}
	MPI_Request req;
	MPI_Request req2;
	MPI_Request req3;
	MPI_Status status;
	if (world_rank == 0){
		for (i = 0; i < 500; i++){
			for (j = 0; j < 500; j++)
				for (k = 0; k < 500; k++)
					X[i][j] += A[i][k] * B[k][j]; 
			MPI_Isend(&X[i], 500, MPI_DOUBLE, 3, 0, MPI_COMM_WORLD, &req);
		}
	}else if (world_rank == 1){
		for (i = 0; i < 500; i++){
			for (j = 0; j < 500; j++)
				for (k = 0; k < 500; k++)
					Y[i][j] += C[i][k] * D[k][j]; 
			MPI_Isend(&Y[i], 500, MPI_DOUBLE, 3, 0, MPI_COMM_WORLD, &req);
		}
	}else if (world_rank == 2){
		for (i = 0; i < 500; i++){
			for (j = 0; j < 500; j++)
				for (k = 0; k < 500; k++)
					Z[i][j] += E[i][k] * F[k][j]; 
			MPI_Isend(&Z[i], 500, MPI_DOUBLE, 3, 0, MPI_COMM_WORLD, &req);
		}
	}else{
		double t[500], u[500], v[500], t2[500], u2[500], v2[500];
		MPI_Irecv(&t, 500, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &req);
		MPI_Irecv(&u, 500, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &req2);
		MPI_Irecv(&v, 500, MPI_DOUBLE, 2, 0, MPI_COMM_WORLD, &req3);
		MPI_Wait(&req, &status);
		MPI_Wait(&req2, &status);
		MPI_Wait(&req3, &status);
		for (i = 0; i < 499; i++){
			if (i % 2 == 0){
				MPI_Irecv(&t2, 500, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &req);
				MPI_Irecv(&u2, 500, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &req2);
				MPI_Irecv(&v2, 500, MPI_DOUBLE, 2, 0, MPI_COMM_WORLD, &req3);
			}else{
				MPI_Irecv(&t, 500, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &req);
				MPI_Irecv(&u, 500, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &req2);
				MPI_Irecv(&v, 500, MPI_DOUBLE, 2, 0, MPI_COMM_WORLD, &req3);
			}
			for (j = 0; j < 500; j++){
				for (k = 0; k < 500; k++)
					if (i % 2 == 0) W[i][j] += (t[k] + u[k] + v[k]) * T[k][j]; 
					else W[i][j] += (t2[k] + u2[k] + v2[k]) * T[k][j]; 

			}
			MPI_Wait(&req, &status);
			MPI_Wait(&req2, &status);
			MPI_Wait(&req3, &status);
		}
		printf("Done!\n");
	}
	// Finalize the MPI environment.
	MPI_Finalize();
}

// ulimit -s unlimited
// mpirun -n 4 ./a.out
