#include <stdio.h>
#include <stdlib.h>

void tar1(){sleep(1); printf("Tarefa 1\n"); fflush(0);}
void tar2(){sleep(2); printf("Tarefa 2\n"); fflush(0);}
void tar3(){sleep(10); printf("Tarefa 3\n"); fflush(0);}
void tar4(){sleep(10); printf("Tarefa 4\n"); fflush(0);}
void tar5(){sleep(5); printf("Tarefa 5\n"); fflush(0);}
void tar6(){sleep(6); printf("Tarefa 6\n"); fflush(0);}

int main(void){

    
    #pragma omp parallel
    #pragma omp single nowait
    {
        printf("Seq1\n"); fflush(0);

        #pragma omp task
        {
            #pragma omp task 
            tar1();
            #pragma omp task 
            tar2();

            #pragma omp taskwait
            printf("Seq2\n");fflush(0);

            #pragma omp task 
            tar5();
            #pragma omp task 
            tar6();

        }

        #pragma omp task
        {
            #pragma omp task
            tar3();
            #pragma omp task 
            tar4();

            #pragma omp taskwait
            printf("Seq3\n");fflush(0);
        }


        #pragma omp taskwait
    }

    return 0;

}
