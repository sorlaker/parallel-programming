#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

int main(void) 
{
    char buscaelem[100];
    printf("Hello. What will u search for? from 1 to %d\n", 1000*1000);
    //scanf("%s", &name);  - deprecated
    fgets(buscaelem,100,stdin);
    printf("Searching for elem: %s\n", buscaelem);

    int elem = atoi(buscaelem);


    int * a = (int *) malloc(1000*1000*sizeof(int));
    int encontrou = 0;

    int i, j;
    for (i = 0; i < 1000; i++)
        for (j = 0; j < 1000; j++)
            a[i*1000 + j] = i*1000 + j + 1;

    int tier = 1000/4;
    #pragma omp parallel private(i, j) shared(elem, a, encontrou, tier) num_threads(4)
    {
        int tid = omp_get_thread_num();
        int i, j, start, end;
        start = tier * tid;
        end = tier * tid + tier;
        printf("Thread %d from %d to %d\n", tid, start, end); fflush(0);
        for (i = start; i < end; i++){
            if (encontrou == 1) break;
            for (j = 0; j < 1000; j++){
                if (encontrou == 1) break;
                if (a[i*1000 + j] == elem) {
                    encontrou = 1;
                    printf("Thread %d encontrou o elemento!\n", tid); fflush(0);
                }
            }
        }
    }

    printf("fim\n");


    return 0;
}
