// 2 processos MPI, cada um criando um grupo com um processo
// rank0 cria um grupo com um processo que executa worker1
// rank1 cria um grupo com um processo que executa worker2

/* manager */ 
#include <stdio.h>
#include "mpi.h" 
int main(int argc, char *argv[]) 
{ 
   int world_size, universe_size, *universe_sizep, flag; 
   MPI_Comm everyone1;           /* intercommunicator 1 */
   MPI_Comm everyone2;
   int X;
   int rank; 
   MPI_Status status; 
MPI_Init(&argc, &argv); 
   MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
printf("PASSEI 1 world_size=%d\n",world_size);
fflush(stdout); 
if (world_size != 2)    error("erro: sao 2 processos"); 
 
/*  
    * Now spawn the workers. Note that there is a run-time determination 
    * of what type of worker to spawn, and presumably this calculation must 
    * be done at run time and cannot be calculated before starting 
    * the program. If everything is known when the application is  
    * first started, it is generally better to start them all at once 
    * in a single MPI_COMM_WORLD.  
    */ 
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);
   if (rank==0) {

      printf("principal rank=0 ANTES DA CRIACAO 1 grupo com 1 processo\n");
      fflush(stdout);

      MPI_Comm_spawn("worker1", MPI_ARGV_NULL, 1,  
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone1,  
             MPI_ERRCODES_IGNORE);
      printf("principal rank=0 criou 1 grupo com 1 processo\n");
      fflush(stdout);

   }  

   else {
      MPI_Comm_spawn("worker2", MPI_ARGV_NULL, 1,
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone2,
             MPI_ERRCODES_IGNORE);
      printf("principal rank=0 criou 1 grupo com 1 processo\n");
      fflush(stdout);
   }  


   /* 
    * Parallel code here. The communicator "everyone" can be used 
    * to communicate with the spawned processes, which have ranks 0,.. 
    * MPI_UNIVERSE_SIZE-1 in the remote group of the intercommunicator 
    * "everyone". 
    */

   if (rank ==0) {

      X=3;
      MPI_Send(&X,1,MPI_INT,0,11,everyone1);
      printf("rank=%d ENVIOU X=3 para 0 de grupo1\n",rank);
      fflush(stdout);
      MPI_Recv(&X,1,MPI_INT,0,12,everyone1,&status);
      printf("RECEBEU X=%d de 0 de grupo1\n",X);
      fflush(stdout);

   }


   else {
      X=7;
      MPI_Send(&X,1,MPI_INT,0,11,everyone2);
      printf("ENVIOU X=7 para 0 de grupo2\n");
      fflush(stdout);
      MPI_Recv(&X,1,MPI_INT,0,12,everyone2,&status);
      printf("RECEBEU X=%d de 0 de grupo2\n",X);
      fflush(stdout);
   }

	   printf ("MAIN TESTE\n");
   fflush(stdout);  
 
MPI_Finalize(); 
   return 0; 
} 



 

