// 1 processo MPI, criando dois grupos com um processo cada
// grupo1 -comunicador everyone1 executa worker_teste3a
// grupo2 -comunicador everyone2 executa worker_teste3b
// o processo rank0 do grupo1 envia uma mensagem para o processo rank1 do grupo1
// o processo rank0 do grupo2 envia uma mensagem para o processo rank1 do grupo2

/* manager */ 
#include <stdio.h>
#include "mpi.h" 
int main(int argc, char *argv[]) 
{ 
//   int world_size, universe_size, *universe_sizep, flag; 
   int world_size; 
   MPI_Comm everyone1;           /* intercommunicator 1 */
   MPI_Comm everyone2;
   int X,Y1,Y2,Z1,Z2;
   int rank; 
   MPI_Status status; 
MPI_Init(&argc, &argv); 
   MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
printf("PASSEI 1 world_size=%d\n",world_size);
fflush(stdout); 
if (world_size != 1)    error("erro: deverá ser apenas 1 processo"); 
 
/*  
    * Now spawn the workers. Note that there is a run-time determination 
    * of what type of worker to spawn, and presumably this calculation must 
    * be done at run time and cannot be calculated before starting 
    * the program. If everything is known when the application is  
    * first started, it is generally better to start them all at once 
    * in a single MPI_COMM_WORLD.  
    */ 
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);
printf("PROGRAMA PRINCIPAL rank=%d\n",rank);
fflush(stdout); 
      MPI_Comm_spawn("worker_teste3_a", MPI_ARGV_NULL, 2,  
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone1,  
             MPI_ERRCODES_IGNORE); 
      MPI_Comm_spawn("worker_teste3_b", MPI_ARGV_NULL, 2,
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone2,
             MPI_ERRCODES_IGNORE);

      X=3;
      MPI_Send(&X,1,MPI_INT,0,11,everyone1);
      printf("rank=%d ENVIOU X=3 para 0 de grupo1\n",rank);
      fflush(stdout);
      X=5;
      MPI_Send(&X,1,MPI_INT,1,11,everyone1);
      printf("rank=%d ENVIOU X=5 para 0 de grupo1\n",rank);
      fflush(stdout);
      X=7;
      MPI_Send(&X,1,MPI_INT,0,11,everyone2);
      printf("ENVIOU X=7 para 0 de grupo2\n");
      fflush(stdout);
      X=10;
      MPI_Send(&X,1,MPI_INT,1,11,everyone2);
      printf("ENVIOU X=10 para 0 de grupo2\n");
      fflush(stdout);
      MPI_Recv(&Y1,1,MPI_INT,0,12,everyone1,&status);
      printf("main RECEBEU Y1=%d de 0 de grupo1\n",Y1);
      fflush(stdout);
      MPI_Recv(&Y2,1,MPI_INT,1,12,everyone1,&status);
      printf("main RECEBEU Y2=%d de 0 de grupo1\n",Y2);
      fflush(stdout);
      MPI_Recv(&Z1,1,MPI_INT,0,12,everyone2,&status);
      printf("RECEBEU Z1=%d de 0 de grupo2\n",Z1);
      fflush(stdout);
      MPI_Recv(&Z2,1,MPI_INT,1,12,everyone2,&status);
      printf("RECEBEU Z2=%d de 0 de grupo2\n",Z2);
      fflush(stdout);
   printf ("MAIN TESTE\n");
   fflush(stdout);  
 
MPI_Finalize(); 
   return 0; 
} 



 

