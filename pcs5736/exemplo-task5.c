//uma regiao de secoes paralelas
// cada secao contem uma task
// cada task contem uma rgiao paralela para ser executada por 2 threads
// as tasks sao executadas simultaneamente, mas as regioes paralelas sao sequencializadas
#include <stdio.h>
#include <omp.h>
int i;
int main()
{
   omp_set_nested(1);
   printf("TESTE %d\n",omp_get_thread_num());
   fflush(stdout);
   omp_set_num_threads(6);
  #pragma omp parallel sections
  {
    #pragma omp section
    {   
           printf("SECTION0 %d\n",omp_get_thread_num());
           fflush(stdout);
     #pragma omp task private(i) 
       {
           printf("TASK0 %d\n",omp_get_thread_num());
           fflush(stdout);
           #pragma omp parallel for num_threads(2)
            for(i=0;i<100;i++) {
               printf("task0 thread %d\n",omp_get_thread_num());
               fflush(stdout);
             }
            
           printf("FIM TASK0 %d\n",omp_get_thread_num());
           fflush(stdout);
       }
           printf("FIM SECTION0 %d\n",omp_get_thread_num());
           fflush(stdout);
     }
    #pragma omp section
    { 
           printf("SECTION1 %d\n",omp_get_thread_num());
           fflush(stdout);
     #pragma omp task private(i)
       {
           printf("TASK1 %d\n",omp_get_thread_num());
           fflush(stdout);
           #pragma omp parallel for num_threads(2)
            for(i=0;i<100;i++) {
                printf("task1 thread %d\n",omp_get_thread_num());
                fflush(stdout);
             }
            
           printf("FIM TASK1 %d\n",omp_get_thread_num());
           fflush(stdout);
       }
           printf("FIM SECTION1 %d\n",omp_get_thread_num());
           fflush(stdout);
     }
    } 
   printf("XXXX %d\n",omp_get_thread_num());
   fflush(stdout);
}

