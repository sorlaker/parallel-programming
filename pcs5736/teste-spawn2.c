// 1 processo MPI, cada um criando um grupo com dois processos
// grupo1 -comunicador everyone1 executa worker_teste2_a
// grupo2 -comunicador everyone2 executa worker_teste2_b

/* manager */ 
#include <stdio.h>
#include "mpi.h" 
int main(int argc, char *argv[]) 
{ 
//   int world_size, universe_size, *universe_sizep, flag; 
   int world_size; 
   MPI_Comm everyone1;           /* intercommunicator 1 */
   MPI_Comm everyone2;
   int X,Y1,Y2,Z1,Z2;
   int rank; 
   MPI_Status status; 
MPI_Init(&argc, &argv); 
   MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
printf("PASSEI 1 world_size=%d\n",world_size);
fflush(stdout); 
if (world_size != 1)    error("erro: deverá ser apenas 1 processo"); 
 
/*  
    * Now spawn the workers. Note that there is a run-time determination 
    * of what type of worker to spawn, and presumably this calculation must 
    * be done at run time and cannot be calculated before starting 
    * the program. If everything is known when the application is  
    * first started, it is generally better to start them all at once 
    * in a single MPI_COMM_WORLD.  
    */ 
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);
printf("PROGRAMA PRINCIPAL rank=%d\n",rank);
fflush(stdout); 
      MPI_Comm_spawn("worker_teste2_a", MPI_ARGV_NULL, 2,  
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone1,  
             MPI_ERRCODES_IGNORE); 
      MPI_Comm_spawn("worker_teste2_b", MPI_ARGV_NULL, 2,
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone2,
             MPI_ERRCODES_IGNORE);

   /* 
    * Parallel code here. The communicator "everyone" can be used 
    * to communicate with the spawned processes, which have ranks 0,.. 
    * MPI_UNIVERSE_SIZE-1 in the remote group of the intercommunicator 
    * "everyone". 
    */
      X=2;
      MPI_Send(&X,1,MPI_INT,0,11,everyone1);
      printf("rank=%d ENVIOU X=%d para 0 de grupo1\n",rank,X);
      fflush(stdout);
      X=5;
      MPI_Send(&X,1,MPI_INT,1,11,everyone1);
      printf("rank=%d ENVIOU X=%d para 1 de grupo1\n",rank,X);
      fflush(stdout);
      X=7;
      MPI_Send(&X,1,MPI_INT,0,11,everyone2);
      printf("rank=%d ENVIOU X=%d para 0 de grupo2\n",rank,X);
      fflush(stdout);
      X=10;
      MPI_Send(&X,1,MPI_INT,1,11,everyone2);
      printf("rank=%d ENVIOU X=%d para 1 de grupo2\n",rank,X);
      fflush(stdout);
      MPI_Recv(&Y1,1,MPI_INT,0,12,everyone1,&status);
      printf("main RECEBEU Y1=%d de 0 de grupo1\n",Y1);
      fflush(stdout);
      MPI_Recv(&Y2,1,MPI_INT,1,12,everyone1,&status);
      printf("main RECEBEU Y2=%d de 0 de grupo1\n",Y2);
      fflush(stdout);
      MPI_Recv(&Z1,1,MPI_INT,0,12,everyone2,&status);
      printf("RECEBEU Z1=%d de 0 de grupo2\n",Z1);
      fflush(stdout);
      MPI_Recv(&Z2,1,MPI_INT,1,12,everyone2,&status);
      printf("RECEBEU Z2=%d de 0 de grupo2\n",Z2);
      fflush(stdout);
   printf ("MAIN TESTE\n");
   fflush(stdout);  
 
MPI_Finalize(); 
   return 0; 
} 



 

