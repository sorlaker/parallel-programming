// 2 tasks com parallel for 
// tasks sao executadas sequencialmente
// loops sao paralelizados
#include <stdio.h>
#include <omp.h>
int i;
int main()
{

     printf("TESTE %d\n",omp_get_thread_num());
     fflush(stdout);
     omp_set_nested(1);  //permite aninhamento de paralelismo
     omp_set_num_threads(4);
     #pragma omp task private(i) 
       {
           printf("TASK0 %d\n",omp_get_thread_num());
           fflush(stdout);
           #pragma omp parallel for num_threads(2)
            for(i=0;i<100;i++) {
               printf("task0 thread %d i=%d\n",omp_get_thread_num(),i);
               fflush(stdout);
             }
            
       }
     #pragma omp task private(i)
       {
           printf("TASK1 %d\n",omp_get_thread_num());
           fflush(stdout);
           #pragma omp parallel for num_threads(2)
            for(i=0;i<100;i++) {
                printf("task1 thread %d i=%d\n",omp_get_thread_num(),i);
                fflush(stdout);
             }
            
       }
     printf("XXXX %d\n",omp_get_thread_num());
     fflush(stdout);
}

