// uma regiao de secoes paralelas, contendo duas secoes
// cada secao executa uma task
// tasks sao executadas paralelamente 
#include <stdio.h>
#include <omp.h>
int i;
int main()
{

   printf("TESTE %d\n",omp_get_thread_num());
   fflush(stdout);
  #pragma omp parallel sections
  {
    #pragma omp section
    {   
           printf("SECTION0 %d\n",omp_get_thread_num());
           fflush(stdout);
     #pragma omp task private(i)  
       {
           printf("TASK0 %d\n",omp_get_thread_num());
           fflush(stdout);
           for(i=0;i<1000;i++) {
                printf("task0 thread %d\n",omp_get_thread_num());
                fflush(stdout);
             }
            
       }
       #pragma omp taskwait
           printf("FIM SECTION0 %d\n",omp_get_thread_num());
           fflush(stdout);
     }
    #pragma omp section
    { 
           printf("SECTION1 %d\n",omp_get_thread_num());
           fflush(stdout);
     #pragma omp task private(i)
       {
           printf("TASK1 %d\n",omp_get_thread_num());
           fflush(stdout);
           for(i=0;i<1000;i++) {
                printf("task1 thread %d\n",omp_get_thread_num());
                fflush(stdout);
             }
            
       }
       #pragma omp taskwait
           printf("FIM SECTION1 %d\n",omp_get_thread_num());
           fflush(stdout);
     }
    } 
   printf("XXXX %d\n",omp_get_thread_num());
   fflush(stdout);
}

