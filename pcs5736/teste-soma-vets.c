// 2 processos MPI, cada um criando um grupo com um processo
/* manager */ 
#include <stdio.h>
#include "mpi.h" 
int main(int argc, char *argv[]) 
{ 
//   int world_size, universe_size, *universe_sizep, flag; 
   int world_size; 
   MPI_Comm everyone1;           /* intercommunicator 1 */
   MPI_Comm everyone2;
   int SOMA_A,SOMA_B;
   int rank; 
   MPI_Status status; 
MPI_Init(&argc, &argv); 
   MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
printf("PASSEI 1 world_size=%d\n",world_size);
fflush(stdout); 
if (world_size != 1)    error("erro: deverá ser apenas 1 processo"); 
 
/*  
    * Now spawn the workers. Note that there is a run-time determination 
    * of what type of worker to spawn, and presumably this calculation must 
    * be done at run time and cannot be calculated before starting 
    * the program. If everything is known when the application is  
    * first started, it is generally better to start them all at once 
    * in a single MPI_COMM_WORLD.  
    */ 
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);
      MPI_Comm_spawn("worker-soma-a", MPI_ARGV_NULL, 2,  
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone1,  
             MPI_ERRCODES_IGNORE); 
      MPI_Comm_spawn("worker-soma-b", MPI_ARGV_NULL, 2,
             MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone2,
             MPI_ERRCODES_IGNORE);

      MPI_Recv(&SOMA_A,1,MPI_INT,0,12,everyone1,&status);
      printf("RECEBEU SOMA_A=%d de 0 de grupo2\n",SOMA_A);
      fflush(stdout);
      MPI_Recv(&SOMA_B,1,MPI_INT,0,12,everyone2,&status);
      printf("RECEBEU SOMA_B=%d de 0 de grupo2\n",SOMA_B);
      fflush(stdout);
  
 printf ("MAIN TESTE\n");
   fflush(stdout);  
 
MPI_Finalize(); 
   return 0; 
} 



 

