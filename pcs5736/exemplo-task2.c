// 2 tasks  - executadas sequencialmente
#include <stdio.h>
#include <omp.h>
int i;
int main()
{

     printf("TESTE %d\n",omp_get_thread_num());
     fflush(stdout);
     #pragma omp task private(i) 
       {
           printf("TASK0 %d\n",omp_get_thread_num());
           fflush(stdout);
           for(i=0;i<1000;i++) {
               printf("task0 thread %d\n",omp_get_thread_num());
               fflush(stdout);
           }
       printf("PASSEI %d\n",omp_get_thread_num());
       fflush(stdout);            
       }
     #pragma omp task private(i)
       {
           printf("TASK1 %d\n",omp_get_thread_num());
           fflush(stdout);
           for(i=0;i<1000;i++) {
                printf("task1 thread %d\n",omp_get_thread_num());
                fflush(stdout);
           }
       }
     printf("XXXX %d\n",omp_get_thread_num());
     fflush(stdout);
}

