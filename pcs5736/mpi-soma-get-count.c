/*
 Soma de elementos de um vetor usando MPI_Send e MPI_Get_count.
 Numero de elentos do vetor: multiplo de numero de processos
 */

#include <mpi.h>
#include <stdio.h>

main(argc, argv)

int			argc;
char			*argv[];

{
	int		n,n_nos, rank;
	MPI_Status	status;
        int inicio,fim,soma_parcial,i,k;
        int soma_total,soma;
        int *vetor; 
/*
 * Initialize MPI.
 */
	MPI_Init(&argc, &argv);
/*
 * Error check the number of processes.
 * Determine my rank in the world group.
 * The sender will be rank 0 and the receiver, rank 1.
 */
	MPI_Comm_size(MPI_COMM_WORLD, &n_nos);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        if (rank==0) {
          n=500;
          vetor=calloc(n,sizeof(int));
          for(i=0;i<n;i++)
            vetor[i]=1;
          k=n/n_nos;
          for (i=1;i<n_nos;i++)
            MPI_Send(&vetor[0]+k*i,k,MPI_INT,i,10,MPI_COMM_WORLD);
        }
        else {
          MPI_Probe(0,10,MPI_COMM_WORLD,&status);
          MPI_Get_count(&status,MPI_INT,&k);
          vetor=calloc(k,sizeof(int));
          MPI_Recv(vetor,k,MPI_INT,0,10,MPI_COMM_WORLD,&status);
        printf("k recebidos=%d\n",k);
        fflush(stdout);
        }
        soma_parcial=0;
        for(i=0;(i<k);i++)
          soma_parcial+=vetor[i];
        printf("soma_parcial=%d\n",soma_parcial);
        fflush(stdout);
        if (rank==0) {
           soma_total=soma_parcial;
           for(i=1;i<n_nos;i++){
             MPI_Recv(&soma,1,MPI_INT,MPI_ANY_SOURCE,11,MPI_COMM_WORLD,&status);
             soma_total+=soma;}
           printf("RESULTADO=%d\n",soma_total);
           fflush(stdout); 
           }
        else {
           MPI_Send(&soma_parcial,1,MPI_INT,0,11,MPI_COMM_WORLD);
             }
	MPI_Finalize();
	return(0);
}
